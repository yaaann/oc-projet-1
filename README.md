# oc-projet-1

Enoncé du projet :

Intégrez la maquette du site d'une agence web
____________________________________________________________________________________________________________________________________________________________________
TL ; DR

* J'ai à ma disposition une maquette numérique à reproduire à l'identique en HTML et CSS.
* Utilisation d'un Framework interdite. Le site devra être responsive de 2000 à 300 px.
* Le filtre du portfolio et le formulaire de contact ne sont pas foncionnels.

____________________________________________________________________________________________________________________________________________________________________

Enoncé

Vous venez d'être embauché(e) par une agence Web, la WebAgency, qui réalise des sites web pour différents clients. Sophie, la directrice technique de l’agence vous informe que le site actuel est vieillissant et que son équipe a peu de temps pour travailler dessus.

Elle vous propose donc, pour votre première mission, de vous occuper de la refonte du site de l'agence.

Voici le mail que vous recevez à votre arrivée le premier jour.

Bonjour et bienvenue à la WebAgency.
J’espère que ta première journée se passe bien !
Pour débuter chez nous, je souhaite que tu t’occupes de la refonte de notre site, qui n’a pas été mis à jour depuis plusieurs mois.
Voici quelques informations dont tu auras besoin pour te lancer :
le site doit tenir sur une page avec un menu qui reste visible, en haut de la page, même lorsque l'on se déplace dans la page ;
les différentes sections attendues sont :

* un premier écran d'accueil de bienvenue, très visuel ;
* la liste des services offerts par l'agence ;
* des exemples de projets déjà réalisés ;
* une carte avec un formulaire de contact (on ne vous demande pas de faire marcher le formulaire, juste de l'afficher).


Le site devra être réalisé en HTML et CSS (sans framework tel que Bootstrap).
Par ailleurs, j’aimerais également que tu rédiges le texte de présentation de WebAgency.
Pour que tu ne partes pas de rien dans la refonte du site, notre graphiste, Mathieu a réalisé des maquettes que tu trouveras ci-dessous.
Voici les différentes sections :

Maquette 1 Maquette 2 Maquette 3 Maquette 4

Tu trouveras également ci-joint le premier découpage des fichiers de la maquette. Il ne reste plus que l’intégration à réaliser.
Je te laisse choisir l’hébergeur pour me présenter la nouvelle version du site. Il peut s’agir d’Heroku, Hostinger, Free, Nexgate, GitHub Pages, Google Sites ou tout autre hébergeur de ton choix.
Enfin, j’aimerais que tu crées un support de présentation comprenant les points suivants :

1. ce que tu as compris du rôle d'intégrateur web et en quoi ta réalisation répond bien à ce rôle ;
2. les difficultés rencontrées et comment tu les as résolues ;
3. les perspectives d’amélioration : comment ferais-tu évoluer l’application si tu avais été libre d'en fixer les contraintes ?


Merci,

Sophie
_____________________________________________________________________________________________________________________________________________________________________
Référentiel d'évaluation
Définir le contenu d'une page web à partir d'une maquette

1. L'ensemble du contenu défini par la maquette est présent
2. Le contenu texte est pertinent et grammaticalement correct
3. L'aspect visuel correspond à la maquette sur écran d'ordinateur
4. La maquette est adaptée de manière pertinente sur un terminal mobile


Les sliders et onglets ne sont pas nécessairement fonctionnels. JavaScript n'est pas requis pour ce projet.
Coder la structure d'une page web en HTML

1. La page HTML passe la validation w3c HTML sans erreur
2. Les balises HTML sont définies avec la sémantique HTML 5 pertinente
3. Les icônes sont crées avec un outil pertinent, type font-awesome


Coder la présentation d'une page web en CSS

1. Le code CSS passe la validation w3c CSS
2. Le code CSS est écrit dans un ou plusieurs fichier CSS
3. Aucun code CSS n'est appliqué via un attribut style dans une balise HTML
4. La zone avec le menu reste visible, en haut de la page, même lorsque l'on se déplace dans la page


Gérer la responsivité avec les Media Queries

1. Le viewport est configuré dans l'en-tête de la page HTML
2. Au moins Un breakpoint pertinent est défini
3. Des règles CSS @media et/ou des feuilles de styles dédiées identifiées par un attribut media sont utilisées
4. Aucun Framework (type Bootstrap) n'est utilisé pour ce projet

Présenter une réalisation de manière professionnelle

1. La soutenance est préparée
2. La tenue et l'attitude sont appropriées
3. Le support de présentation est soigné, respecte la structure imposée et le temps imparti
4. Le temps de présentation est respecté
5. Les réponses aux questions sont pertinentes




